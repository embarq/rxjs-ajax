import { fromPromise } from 'rxjs/observable/fromPromise';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { of } from 'rxjs/observable/of';
import { tap, map, switchMap, catchError } from 'rxjs/operators';

const API_URL = 'https://rx-microadmin.herokuapp.com';

const httpGet$ = url => {
  const headers = new Headers();

  headers.append("Content-Type", "application/json");
  headers.append("Accept", "application/json");

  const options = {
    method: 'GET',
    headers: headers,

    mode: 'cors',
    credentials: 'omit'
  };


  const fetchPromise = fetch(new Request(url, options)).then(res => res.json());
  return fromPromise(fetchPromise);
};



const getOrderItems$ = order => {
  return httpGet$(`${ API_URL }/order-items?order_id=${ order.id }`)
    .pipe(
      tap(res => {
        console.log('Result', res)
      }),
      map(items => ({
        ...order,
        items
      })),
      catchError(err => {
        console.log(err);
        return of({
          ...order,
          items: null
        })
      })
    )
}

const getUserOrders$ = user => {
  return httpGet$(`${ API_URL }/orders?user_id=${ user.id }`)
    .pipe(
      switchMap((orders) => {
        const orderItems$ = orders.map(getOrderItems$);
        return forkJoin(orderItems$);
      }),
      map(orders => ({
        ...user,
        orders
      }))
    )
};

export const app = () => {
  console.log('%cApp is running', 'color: green');

  httpGet$(`${ API_URL }/users`)
    .pipe(
      switchMap(users => {
        const orders$ = users.map(getUserOrders$);
        return forkJoin(orders$);
      }),
      tap(console.log)
    )
    .subscribe()
}
